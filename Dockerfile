FROM registry.gitlab.com/xxllnc/zaakgericht/zaken/libraries/zsnl-p5-service_http_library:b731e202

ARG BUILD_TIMESTAMP=undefined
ARG BUILD_REF=master

ENV SERVICE_HOME /opt/zaaksysteem-virus_scanner-service

WORKDIR $SERVICE_HOME

ENV ZSVC_LOG4PERL_CONFIG=/etc/zaaksysteem-virus_scanner-service/log4perl.conf \
    ZSVC_SERVICE_CONFIG=/etc/zaaksysteem-virus_scanner-service/zaaksysteem-virus_scanner-service.conf

COPY Makefile.PL ./
RUN cpanm --installdeps . && rm -rf ~/.cpanm || (cat ~/.cpanm/work/*/build.log && false)

COPY bin ./bin
COPY xt  ./xt
COPY t   ./t
COPY etc  /etc/zaaksysteem-virus_scanner-service

COPY lib ./lib

RUN echo "$BUILD_REF @ $BUILD_TIMESTAMP" > /opt/build-timestamp

CMD starman --preload-app $SERVICE_HOME/bin/zaaksysteem-virus_scanner-service.psgi

EXPOSE 5000
